import store from "../store"

function isInURL (action) {
  return window.location.href.includes(action)
}

function convertToMenuItemName (controller, action){
  const c = controller
  const a = action
  if (c === 'users') {
    if (a === 'lk') return 'lk'
    if (a === 'show') return 'settings_users'
    if (a === 'index') return 'settings_users'
    if (a === 'old') return 'settings_users'
    throw new Error(`Unknown action ${a} in controller ${c}`)
  }
  if (c === 'orders') {
    return 'orders'
  }
  if (c === 'statistics') {
    if (isInURL('trucking_companies')) return 'statistics_by_trucking'
    if (isInURL('companies')) return 'statistics_by_companies'
    if (isInURL('managers')) return 'statistics_by_managers'
    if (isInURL('drivers')) return 'statistics_by_drivers'

    throw new Error(`Unknown action ${a} in controller ${c}`)
  }

  if (c === 'order_money_movements') {
    return 'finances_by_orders'
  }
  if (c === 'user_money_movements' ||
      c === 'salary_moneys') {
    return 'finances_by_salary'
  }
  if (c === 'other_money_movements') {
    return 'finances_others'
  }
  if (c === 'contragent_moneys') {
    if (a === 'index') return 'finances_contragent'

    throw new Error(`Unknown action ${a} in controller ${c}`)
  }

  if (c === 'companies') {
    if (a === 'ours') {
      return 'settings_companies'
    }
    return c
  }

  if (c === 'drivers') {
    return c
  }

  if (c === 'documents') {
    if (a === 'exportable') {
      return 'export_to_one_s'
    } else {
      return c
    }
  }

  if (c === 'attachments') {
    return c
  }

  if (c === 'doc_templates') {
    return c
  }


  if (c === 'client_settings') {
    if (a === 'show') {
      return 'setting_common'
    }
    if (a === 'balance') {
      return 'balance'
    }
  }

  if (c === 'guide_pages') {
    return 'help'
  }

  throw new Error('Unknown controller')
}

const controller = JSDATA.controller_name
const action = JSDATA.action_name

if (controller !== "spa") {
  store.commit('menu/setCurrentMenuItem', convertToMenuItemName(controller, action))
}
