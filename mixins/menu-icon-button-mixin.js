import $s from '../store'

export default {
  props: {
    name: {
      type: String,
      required: true
    },
    iconClass: {
      type: String,
      required: false
    },
    iconPath: {
      type: String,
      required: false
    },
    disabled: {
      type: Boolean,
      required: false
    },
    disabledMessage: {
      type: String, 
      required: false
    },
    badgeCount: {
      type: Number | String,
      required: false
    }
  },

  computed: {
    isSelected() {
      return $s.userSelectedMenuItem === this.name;
    },
    isActive () {
      if (this.name == null) {
        throw new Error('No name for button item "' + this.text + '"')
      }
      return this.$store.state.menu.currentMenuItemName === this.name;
    }
  },
  
  methods: {
    toggleSelectedMenuItem() {
      if (this.disabled) {
        if (this.disabledMessage)
          return $.notify(this.disabledMessage, 'info')
        
        return $.notify('Функционал скоро будет доступен', 'info')
      }

      if (this.isSelected) {
        return $s.userSelectedMenuItem = null;
      }
        
      $s.userSelectedMenuItem = this.name;
      $s.isPopupShown = true;
    }
  }
}