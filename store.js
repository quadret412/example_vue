import Vue from "vue";

export default Vue.observable({
  userSelectedMenuItem: null,
  isCompact: false,
  isHamburgerMenuVisible: false,
  isDesktop: true,
  isPopupShown: false
});
