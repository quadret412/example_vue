import Vue from "vue";
import Menu from "./app.vue";
import adapter from "./adater"

Raven.context(() => {
  window.Menu = new Vue(Menu);
  window.Menu.$mount("left-sidebar");
});
