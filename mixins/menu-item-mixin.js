export default {
  props: {
    href: {
      type: String,
      required: true
    },
    text: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true
    }
  },
  
  computed: {
    isActive () {
      if (this.name == null) {
        throw new Error('No name for button item "' + this.text + '"')
      }
      return this.$store.state.menu.currentMenuItemName === this.name;
    }
  }
}