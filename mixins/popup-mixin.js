import $s from './../store'

export default {
  data() {
    return {
      userSelectedMenuItemBeforeClick: null
    }
  },

  methods: {
    beforeClickAnywhereHidePopup() {
      this.userSelectedMenuItemBeforeClick = $s.userSelectedMenuItem
    },
    onClickAnywhereHidePopup(e) {
      const box = this.$el.getBoundingClientRect()

      if (e.x < box.left || e.x > box.right || e.y < box.top  || e.y > box.bottom) {
        if (this.userSelectedMenuItemBeforeClick === $s.userSelectedMenuItem) {
          $s.userSelectedMenuItem = null
        }
      }
    }
  },

  mounted() {
    window.addEventListener('mousedown', this.beforeClickAnywhereHidePopup)
    window.addEventListener('click', this.onClickAnywhereHidePopup)
  },

  beforeDestroy() {
    window.removeEventListener('mousedown', this.beforeClickAnywhereHidePopup)
    window.removeEventListener('click', this.onClickAnywhereHidePopup)
  }
}